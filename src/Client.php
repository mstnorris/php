<?php


namespace Websnap\Php;

use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var string
     */
    private $token;

    public function __construct(string $token = '')
    {
        $this->token = $token;
    }

    public function screenshot(string $url, array $options = []): ResponseInterface
    {
        $urlModel = new Url(
            $url,
            array_merge(
                ['token' => $this->token],
                $options
            )
        );

        $client = new GuzzleClient();
        return $client->get($urlModel->build());
    }
}
