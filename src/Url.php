<?php


namespace Websnap\Php;

class Url
{
    private const API_URL = 'https://img.websnap.app';

    /**
     * @var array
     */
    private $params = [];

    public function __construct(string $url, array $options = [])
    {
        $this->params = array_merge(
            [
                'url' => $url
            ],
            $options
        );
    }

    public function __toString(): string
    {
        return $this->build();
    }

    public function build(): string
    {
        $holder = [];

        foreach ($this->params as $name => $value) {
            if (!$value) {
                continue;
            }

            $holder[$name] = $value;
        }

        return self::API_URL . '?' . http_build_query($holder);
    }
}
