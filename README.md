## Installation
Install with composer: `composer require websnap/php`

## Getting started
1. Register for free at https://www.websnap.app
2. Create a project and copy it's `token`

## Usage
```
$client = new \Websnap\Php\Client('YOUR_TOKEN');
$response = $client->screenshot('https://www.websnap.app');
```

Please see the [documentation](https://www.websnap.app/docs) for further options.